//--------------------------------------------------------------------------------
// Request
//--------------------------------------------------------------------------------
{
  "type":"roomchangerequest",
  "data":
    {
      "room":"1",
      "pos":"3"
    }
}
    <room>
		Identificador da sala
    <pass>
		Identificador da posi��o na sala
		
//--------------------------------------------------------------------------------
// Response
//--------------------------------------------------------------------------------
{
  "type":"roomchangeresponse",
  "data":
    {
      "status":"0"
    }
}
    <status>
        -1: Erro desconhecido
        0: Sucesso
        1: Usu�rio entrou na sala e � o dono
        2: Usu�rio mudou de canto na pr�pria sala
        3: J� existe um jogador na posi��o que ele deseja entrar
        4: N�o est� logado 