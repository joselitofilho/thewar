﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace br.com.thewar.protocol.response
{
    public class ListUsersLoggedResponse
    {
        #region Propriedades
        /// <summary>
        /// 
        /// </summary>
        public List<string> ListUsers { get; set; }
        #endregion
    }
}
