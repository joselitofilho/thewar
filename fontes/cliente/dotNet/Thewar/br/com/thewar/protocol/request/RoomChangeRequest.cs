﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace br.com.thewar.protocol.request
{
    public class RoomChangeRequest
    {
        #region Propriedades
        /// <summary>
        /// 
        /// </summary>
        public int Room { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Pos { get; set; }
        #endregion
    }
}
