﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace br.com.thewar.protocol.response
{
    public class RoomChangeResponse
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Room { get; set; }
        public string Pos { get; set; }
        public string Status { get; set; }
        #endregion
    }
}
