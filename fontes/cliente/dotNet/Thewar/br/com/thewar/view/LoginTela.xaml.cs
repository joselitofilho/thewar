﻿using System;
using System.Windows;
using System.Windows.Controls;
using br.com.thewar;
using br.com.thewar.lang;
using br.com.thewar.model;
using br.com.thewar.protocol.response;
using br.com.thewar.protocolo;

namespace Thewar.br.com.thewar
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class LoginTela : UserControl
    {
        public LoginTela()
        {
            InitializeComponent();

            // Atualizando o contexto com a requisição do login.
            DataContext = new LoginRequest();
        }

        public void processResponse(LoginResponse loginRequest)
        {
            if (loginRequest.Status != 0)
            {
                MessageBox.Show("Error codi: " + loginRequest.Status, "Login", MessageBoxButton.OK, MessageBoxImage.Error);

                // É isso mesmo! Para atualizar o botão precisa de toda essa novela!
                btLogin.Dispatcher.Invoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                    new Action(
                      delegate()
                      {
                          btLogin.IsEnabled = true;
                      }
                  ));
            }
        }

        private void btLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Desabilita o botão de login.
                btLogin.IsEnabled = false;

                // Cria a requisição de login
                var login = DataContext as LoginRequest;
                if (login != null)
                {
                    // Adicionando usuário na sessão.
                    Session.getSession().User = new User()
                                                    {
                                                        Login = new Login()
                                                                    {
                                                                        Nick = login.Nick,
                                                                        Pass = login.Pass
                                                                    }
                                                    };

                    // Envia a requisição.
                    MainManager.Communication.SendObject(login);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
