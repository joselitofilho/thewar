﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace br.com.thewar.protocolo
{
    /// <summary>
    /// 
    /// </summary>
    public class GenericObject
    {
        #region Propriedades
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object Data { get; set; }
        #endregion
    }
}
