package br.com.thewar.server.request;

public class LoginRequest {

	// Constant representing the type of that class.
	public static final String TYPE_NAME = "loginRequest";
	
	// Player's name.
	private String _nick;
	
	public String getNick() {
		return _nick;
	}
	
	public void setNick(String nick) {
		_nick = nick;
	}
}
