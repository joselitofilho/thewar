package br.com.thewar.server.model;

public class PlayerModel {

	private String _nick;
	
	public String getNick() {
		return _nick;
	}
	
	public void setNick(String nick) {
		_nick = nick;
	}
}
