package br.com.thewar.server.request;

public class RoomChangeRequest {

	private Integer room;

	private Integer pos;

	public RoomChangeRequest() {
		// TODO Auto-generated constructor stub
	}

	public Integer getRoom() {
		return room;
	}

	public void setRoom(Integer room) {
		this.room = room;
	}

	public Integer getPos() {
		return pos;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

}
