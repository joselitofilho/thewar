package br.com.thewar.server.lang;

public enum ELoginStatus {
	
	UNKNOW(-1),
	SUCCESS(0),
	USER_ALREADY_LOGGED(1),
	ROOM_FULL(2);

	private int _value; 
			
    ELoginStatus(int value) {
		_value = value;
	}
	
    public int getValue() {
    	return _value;
    }
}
