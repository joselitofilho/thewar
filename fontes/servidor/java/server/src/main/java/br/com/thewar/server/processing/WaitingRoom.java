package br.com.thewar.server.processing;

import java.util.ArrayList;

import br.com.thewar.server.model.PlayerModel;

public class WaitingRoom {

	private ArrayList<PlayerModel> _listPlayers;

	/**
	 * Default constructor.
	 */
	public WaitingRoom() {
		_listPlayers = new ArrayList<PlayerModel>(6);
	}
	
	/**
	 * Adds the player at the next position.
	 * @param player
	 */
	public boolean add(PlayerModel player) {
		if (!isFull())  {
			return _listPlayers.add(player);
		}
		
		return false;
	}
	
	/**
	 * Checks if the wainting room is full.
	 * @return
	 */
	public boolean isFull() {
		return _listPlayers.size() == 6;
	}
	
	/**
	 * Clears the wainting room.
	 */
	public void clean() {
		_listPlayers.clear();
	}
	
	//--------------------------------------------------------------------------------
	// Getters and Setters
	//--------------------------------------------------------------------------------
	
	public ArrayList<PlayerModel> getListPlayers() {
		return _listPlayers;
	}
}
