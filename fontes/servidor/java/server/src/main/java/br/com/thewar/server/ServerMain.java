package br.com.thewar.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.thewar.server.dao.InitialData;
import br.com.thewar.server.lang.ELoginStatus;
import br.com.thewar.server.lang.IObserver;
import br.com.thewar.server.model.PlayerModel;
import br.com.thewar.server.processing.WaitingRoom;
import br.com.thewar.server.protocol.Receiver;
import br.com.thewar.server.request.LoginRequest;

/**
 * The War server class game
 * 
 * @author Bruno Lopes Alcantara Batista
 * @author Joselito Viveiros Nogueira Filho
 * 
 */
public class ServerMain implements Runnable, IObserver {

	/**
	 * LOGIN Constant
	 */
	public static String LOGIN = "login";

	/**
	 * LOGOFF constant
	 */
	public static String LOGOFF = "logoff";

	/**
	 * SET_PLAYER constant
	 */
	public static String SET_PLAYER = "setPlayer";

	/**
	 * CHAT constant
	 */
	public static String CHAT = "chat";

	/**
	 * START_GAME constant
	 */
	public static String START_GAME = "startGame";

	/**
	 * UPDATE_GAME constant
	 */
	public static String UPDATE_GAME = "updateGame";

	// Sentinel variable to while block
	private boolean execute;

	// ServerSocker to receive the client connection
	private ServerSocket serverSocket;

	// Sockect with the client
	private Socket socket;

	// Logger class to log the actions
	private static Logger logger;

	private HashMap<Socket, Receiver> socketReceiver;

	// List of players how are logged.
	private static HashMap<String, PlayerModel> _playerLogged;

	// Instance of Waiting Room.
	private static WaitingRoom _waitingRoom;

	static {
		_playerLogged = new HashMap<String, PlayerModel>();
		_waitingRoom = new WaitingRoom();
	}

	/**
	 * Network server of The War game
	 * 
	 * @param port
	 *            of server will be listening
	 */
	public ServerMain(int port) {

		try {

			// Initialize the main variables
			logger = Logger.getLogger("Server initialized!");
			serverSocket = new ServerSocket(port);
			execute = true;
			socketReceiver = new HashMap<Socket, Receiver>();

		} catch (IOException e) {

			// Register the action on the log and close the application
			logger.log(Level.SEVERE,
					"error to create the server: " + e.getMessage());
			System.exit(-1);

		}

	}

	/**
	 * The method run
	 */
	public void run() {
		/*
		 * When the sentinel variable is true will execute this block. Case
		 * occur a error launch the exception!
		 */
		while (execute) {

			try {

				// Wait the client connect with the server and log the actions
				logger.log(Level.INFO, "Waiting connection...");
				socket = serverSocket.accept();

				// Log the client connection and transfer the socket to receiver
				// object to treat the conversation
				logger.log(Level.INFO,
						"Client " + socket.getRemoteSocketAddress()
								+ " is now connected!");

				// Create a new receiver object to treat this request
				Receiver receiver = new Receiver(socket);
				receiver.attach(this);
				socketReceiver.put(socket, receiver);

				receiver.start();
			} catch (IOException e) {

				// Log the exception
				logger.log(Level.SEVERE, e.getMessage());

			}

		}

	}

	/**
	 * Method that send a message to a list of sockets
	 * 
	 * @param message
	 *            to send
	 * @param sockets
	 *            of session
	 */
	public void sendMessage(String message, List<Socket> sockets) {

		if (sockets != null) {

			for (Socket socket : sockets) {

				try {

					PrintStream printStream = new PrintStream(
							socket.getOutputStream());
					printStream.print(message);
					printStream.flush();

				} catch (IOException e) {

					// Log the exception
					logger.log(Level.SEVERE, e.getMessage());

				}

			}

		}

	}

	public void Update() {
		// Faz nada.
		// TODO: será realmente necessário esse método aqui?
	}

	public void Update(Socket socket) {
		// TODO: descobrir em que momento ele entra aqui.
		Receiver r = socketReceiver.get(socket);

		sendMessage(r.getStateMessage(), r.getStateSockets());
	}

	/**
	 * 
	 * @param nick
	 * @return
	 */
	public static boolean UserLogged(String nick) {
		return _playerLogged.containsKey(nick);
	}

	/**
	 * Perform the login process.
	 * 
	 * @param loginRequest
	 */
	public static ELoginStatus ProcessLogin(LoginRequest loginRequest) {
		ELoginStatus loginStatusReturn = ELoginStatus.UNKNOW;
		String nick = loginRequest.getNick();

		if (_playerLogged.containsKey(nick)) {
			// The player already exists.
			loginStatusReturn = ELoginStatus.USER_ALREADY_LOGGED;
		} else if (!_waitingRoom.isFull()) {

			// Adds the player on the list of players logged in and adds it in the 
			// waiting room.

			// Creating the instance of the player.
			PlayerModel player = new PlayerModel();
			player.setNick(nick);
			
			// Adds the player in the wainting room.
			_waitingRoom.add(player);

			// Adds the player on the list of players logged.
			_playerLogged.put(nick, player);
			
			loginStatusReturn = ELoginStatus.SUCCESS;
		} else {
			// The wainting room is already full.
			loginStatusReturn = ELoginStatus.ROOM_FULL;
		}

		return loginStatusReturn;
	}

	public static void clean() {
		_waitingRoom.clean();

		_playerLogged.clear();
	}

	// --------------------------------------------------------------------------------
	// Getters and setters
	// --------------------------------------------------------------------------------

	public static WaitingRoom getWaitingRoom() {
		return _waitingRoom;
	}

	// --------------------------------------------------------------------------------
	// Main
	// --------------------------------------------------------------------------------

	public static void main(String[] args) {

		// Log object to log events
		Logger logger = Logger.getLogger(ServerMain.class.getName());

		try {

			InitialData.fill();

			// Start the server
			ServerMain server = new ServerMain(1234);
			server.run();

		} catch (Exception e) {

			// Register the action on the log and close the application
			logger.log(Level.SEVERE,
					"Error to initialize the game: " + e.getMessage());
			e.printStackTrace();
			System.exit(-1);

		}

	}
}
