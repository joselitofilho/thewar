package br.com.thewar.server.protocol;

import static org.junit.Assert.*;

import java.net.Socket;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import br.com.thewar.server.dao.LoginDAO;
import br.com.thewar.server.lang.IObserver;
import br.com.thewar.server.model.Login;
import br.com.thewar.server.response.LoginResponse;

import static org.easymock.EasyMock.*;

public class Test_Receiver extends TestCase {

	private Socket _socketMock;
	private LoginDAO _loginDAOMock;
	
	@Before
	public void setUp()
	{
		_socketMock = createMock(Socket.class);
		_loginDAOMock = createMock(LoginDAO.class);
	}
	
	
	private void processLoginMock(Login loginRequest)
	{
		// Preparing.
		// Create the login credential. 
		Login resultLogin = new Login();
		resultLogin.setNick("joselito");
		resultLogin.setPass("1234");
		
		String nick = loginRequest.getNick();
		String pass = loginRequest.getPass();
		
		ResponseCode respCode = ResponseCode.UNKNOW;
		
		// Operation
		expect(_loginDAOMock.load(nick, pass)).andReturn(resultLogin);
		
		respCode = (loginRequest != null) ? ResponseCode.SUCCESS
				: ResponseCode.LOGIN_UNKNOW_USER;
		
		
	}
	
	@Test
	public void Test_processLogin() {
		
		// Preparing
		Receiver receiver = new Receiver(_socketMock);
		Login loginRequest = new Login();
		loginRequest.setNick("joselito");
		loginRequest.setPass("1234");
		
		// Operation
		processLoginMock(loginRequest);
		//receiver.processLogin(loginRequest);
	}

}
