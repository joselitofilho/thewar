package br.com.thewar.server.processing;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import br.com.thewar.server.model.PlayerModel;

public class WaintingRoomTets {

	private WaitingRoom _waintingRoom;

	@Before
	public void SetUp() {
		_waintingRoom = new WaitingRoom();
		_waintingRoom.clean();
	}

	@Test
	public void test_Add() {
		// Preparing
		PlayerModel player = new PlayerModel();
		player.setNick("joselito");

		// Operation
		boolean isAdded = _waintingRoom.add(player);

		// Checks
		Assert.assertTrue(isAdded);
		Assert.assertTrue(_waintingRoom.getListPlayers().size() == 1);
	}

	@Test
	public void test_Add_Full() {
		// NOTE: Tests when a player is added and the room is filled.
		// Preparing
		PlayerModel player1 = new PlayerModel();
		player1.setNick("joselito");
		PlayerModel player2 = new PlayerModel();
		player2.setNick("matheus");
		PlayerModel player3 = new PlayerModel();
		player3.setNick("marcos");
		PlayerModel player4 = new PlayerModel();
		player4.setNick("joão");
		PlayerModel player5 = new PlayerModel();
		player5.setNick("ze");
		PlayerModel player6 = new PlayerModel();
		player6.setNick("bruno");
		PlayerModel player7 = new PlayerModel();
		player7.setNick("last not added");

		// Operation
		_waintingRoom.add(player1);
		_waintingRoom.add(player2);
		_waintingRoom.add(player3);
		_waintingRoom.add(player4);
		_waintingRoom.add(player5);
		_waintingRoom.add(player6);
		boolean isAdded = _waintingRoom.add(player7);

		// Checks
		Assert.assertFalse(isAdded);
	}

	@Test
	public void test_IsFull() {
		// Preparing
		PlayerModel player1 = new PlayerModel();
		player1.setNick("joselito");
		PlayerModel player2 = new PlayerModel();
		player2.setNick("matheus");
		PlayerModel player3 = new PlayerModel();
		player3.setNick("marcos");
		PlayerModel player4 = new PlayerModel();
		player4.setNick("joão");
		PlayerModel player5 = new PlayerModel();
		player5.setNick("ze");
		PlayerModel player6 = new PlayerModel();
		player6.setNick("bruno");

		// Operation
		_waintingRoom.add(player1);
		_waintingRoom.add(player2);
		_waintingRoom.add(player3);
		_waintingRoom.add(player4);
		_waintingRoom.add(player5);
		_waintingRoom.add(player6);

		// Checks
		Assert.assertTrue(_waintingRoom.isFull());
	}

	@Test
	public void test_Clean() {
		// Preparing
		PlayerModel player = new PlayerModel();
		player.setNick("joselito");

		// Operation
		_waintingRoom.add(player);
		_waintingRoom.clean();

		// Checks
		Assert.assertTrue(_waintingRoom.getListPlayers().size() == 0);
	}
}
