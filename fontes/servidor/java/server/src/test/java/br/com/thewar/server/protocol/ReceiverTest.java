package br.com.thewar.server.protocol;

import static org.easymock.EasyMock.createMock;

import java.net.Socket;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import br.com.thewar.server.request.LoginRequest;

public class ReceiverTest {

	// Receptador das mensagens que chegam no servidor.
	private Receiver _receiver;
	// Mock do socket que será utilizado pelo receptor.
	private Socket _socketMock;
	
	@Before
	public void SetUp() {
		_socketMock = createMock(Socket.class);
		_receiver = new Receiver(_socketMock);
	}

	@Test
	public void Test_InterpretData_LoginRequest() {
		// Preparação
		String data = "{\"type\":\"loginRequest\",\"data\":{\"nick\":\"joselito\"}}";
		String expectedNick = "joselito";

		// Operação
		Object loginRequest = _receiver.interpretData(data);

		// Execução
		Assert.assertTrue(loginRequest instanceof LoginRequest);
		Assert.assertEquals(expectedNick,
				((LoginRequest) loginRequest).getNick());
	}

}
