package br.com.thewar.server;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import br.com.thewar.server.lang.ELoginStatus;
import br.com.thewar.server.request.LoginRequest;

public class ServerMainTest {

	@Before
	public void SetUp() {
		ServerMain.clean();
	}
	
	@Test
	public void test_ProcessLogin() {
		// NOTE: Testa se o usuário está sendo logado corretamente.
		// NOTE: Fluxo básico.
		
		// Preparação
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setNick("joselito");
		
		ELoginStatus statusExpected = ELoginStatus.SUCCESS;
		
		// Operação
		ELoginStatus statusActual = ServerMain.ProcessLogin(loginRequest);
		
		// Verificação
		Assert.assertTrue(ServerMain.UserLogged("joselito"));
		Assert.assertEquals(statusExpected, statusActual);
	}

	@Test
	public void test_ProcessLogin_UserAlreadyLogged() {
		// Preparação
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setNick("joselito");
		
		ELoginStatus statusExpected = ELoginStatus.USER_ALREADY_LOGGED;
		
		// Operação
		ServerMain.ProcessLogin(loginRequest);
		ELoginStatus statusActual = ServerMain.ProcessLogin(loginRequest);
		
		// Verificação
		Assert.assertTrue(ServerMain.UserLogged("joselito"));
		Assert.assertEquals(statusExpected, statusActual);
	}
	
	@Test
	public void test_ProcessLogin_RoomFull() {
		// Preparação
		LoginRequest loginRequest1 = new LoginRequest();
		loginRequest1.setNick("joselito");
		LoginRequest loginRequest2 = new LoginRequest();
		loginRequest2.setNick("matheus");
		LoginRequest loginRequest3 = new LoginRequest();
		loginRequest3.setNick("bruno");
		LoginRequest loginRequest4 = new LoginRequest();
		loginRequest4.setNick("gabriel");
		LoginRequest loginRequest5 = new LoginRequest();
		loginRequest5.setNick("arleudo");
		LoginRequest loginRequest6 = new LoginRequest();
		loginRequest6.setNick("hugo");
		LoginRequest loginRequest7 = new LoginRequest();
		loginRequest7.setNick("moises");
		
		ELoginStatus statusExpected = ELoginStatus.ROOM_FULL;
		
		// Operação
		ServerMain.ProcessLogin(loginRequest1);
		ServerMain.ProcessLogin(loginRequest2);
		ServerMain.ProcessLogin(loginRequest3);
		ServerMain.ProcessLogin(loginRequest4);
		ServerMain.ProcessLogin(loginRequest5);
		ServerMain.ProcessLogin(loginRequest6);
		ELoginStatus statusActual = ServerMain.ProcessLogin(loginRequest7);
		
		// Verificação
		Assert.assertTrue(ServerMain.getWaitingRoom().isFull());
		Assert.assertEquals(statusExpected, statusActual);
	}
}
